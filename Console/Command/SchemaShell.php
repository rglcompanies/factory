<?php

App::uses('CakeSchema', 'Model');
App::uses('ConnectionManager', 'Model');

class SchemaShell extends AppShell {

    public $uses = array();

    public function main() {
        $this->help();
    }

    public function help() {
        $this->out('usage: cake Factory.schema copy');
    }

    /**
     * Copies schema from source to target db
     */
    public function copy() {
        $source_db_key = 'default';
        $target_db_key = 'test';

        if (!empty($this->args[0])) {
            $source_db_key = $this->args[0];
        }
        if (!empty($this->args[1])) {
            $target_db_key = $this->args[1];
        }


        if ($source_db_key == $target_db_key)
            throw new Exception('Source and target databases cannot match');

        $source_db = ConnectionManager::getDataSource($source_db_key);
        $target_db = ConnectionManager::getDataSource($target_db_key);

        $source_schema = $this->_getSchema($source_db, $source_db_key);
        $target_schema = $this->_getSchema($target_db, $target_db_key);

        $drop_sql = $target_db->dropSchema($target_schema);
        if ($drop_sql) {
            $this->out('Clearing target database');
            $target_db->execute($drop_sql);
        }

        $target_schema->tables = $source_schema->tables;

        $create_sql = $target_db->createSchema($target_schema);
        if ($create_sql) {
            $this->out('Copying from ' . $source_db_key . ' to ' . $target_db_key);
            $target_db->execute($create_sql);
        }
        $this->out('Done');
    }

    private function _getSchema($db, $db_key) {
        $schema = new CakeSchema(array(
            'name' => $db,
            'connection' => $db_key
        ));

        $tables = $schema->read();
        $tables = $tables['tables'];
        if (isset($tables['missing'])) {
            $tables = am($tables, $tables['missing']);
            unset($tables['missing']);
        }

        $schema->tables = $tables;

        return $schema;
    }
}