# Factory layer for Phactory lib #

## Usage ##

### Defining factories ###

Select table (singular name without prefix) and define
- Factory::defineFor('user');
- Factory::define('student', array(
    'column'=>'value',
    'username' => 'aaron'
  ));
- Factory::define('admin', array('username'=>'admin'));

### Creating factories ###

Factory::create('student', array('override' => 'value'));

### Clearing database ###

Use: Factory::reset() to destroy all created records

### Cloning schema to test database ###

use shell task:
- cake Facory.schema copy
Additionally you can specify source and target settings from database.php:
- cake Facory.schema copy source_config target_config