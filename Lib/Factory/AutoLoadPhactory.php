<?php

class AutoLoadPhactory {
    public static function load($class_name) {
        $class_path = str_replace("\\", DS, $class_name);
        return App::import('Vendor', 'Factory.phactory' . DS . 'lib' . DS . $class_path);
    }
}
spl_autoload_register(array('AutoLoadPhactory', 'load'));