<?php

App::import('Lib', 'Factory.Factory/AutoLoadPhactory');
App::uses('Folder', 'Utility');

class Factory {
    private static $_builder;
    private static $_current_table;
    private static $_definitions = array();
    private static $_db_key = 'test';
    private static $_db_key_changed = true;

    /**
     * Singleton class
     */
    private function __construct() { }

    /**
     * Selects table to add definitions
     * @static
     * @param $table
     */
    public static function defineFor($table) {
        self::$_current_table = self::_getTablePrefix() . $table;
    }

    /**
     * Adds definition to $_current_table
     *
     * @static
     * @param $name
     * @param array $defaults
     * @param array $associations
     * @throws Exception if $_current_table is null (use defineFor())
     */
    public static function define($name, $defaults = array()) {
        if (self::$_current_table === null) {
            throw new Exception('No table selected. Use defineFor() before any definitions.');
        }

        if (isset(self::$_definitions[$name])) {
            throw new Exception("Factory name: is already in use");
        }

        self::$_definitions[$name] = array(
            'table' => self::$_current_table,
            'defaults' => $defaults,
        );
    }

    public static function get($name, $overrides = array()) {
        $builder = self::getBuilder();
        $definition = self::$_definitions[$name];
        $attributes = am($definition['defaults'], $overrides);
        return $attributes;
    }

    public static function create($name, $overrides = array()) {
        $builder = self::getBuilder();
        $definition = self::$_definitions[$name];
        $attributes = am($definition['defaults'], $overrides);

        $builder->define($definition['table'], $attributes);
        return $builder->create($definition['table'], $attributes)->toArray();
    }

    /**
     * Changes the database config key used in Config/database.php
     * default is 'test'
     *
     * @static
     * @param $key
     */
    public static function setDbKey($key) {
        self::$_db_key = $key;
        self::$_db_key_changed = true;
    }

    /**
     * Returns the factory object
     *
     * @return Phactory\Sql\Phactory
     */
    public static function getBuilder() {
        if (self::$_builder === null || self::$_db_key_changed) {
            self::$_db_key_changed = false;
            self::_init();
        }

        return self::$_builder;
    }

    public static function reset($tables = array()) {
        // init factories
        self::getBuilder();

        // generate table list
	    if (is_string($tables)) {
		    $tables = array($tables);
	    }

	    if (empty($tables)) {
	        foreach (self::$_definitions as $def) {
	            $tables[] = $def['table'];
	        }
	    }
        $tables = array_unique($tables);

        // truncate tables
        $db = self::_getConnection();
        foreach ($tables as $table) {
            $table = \Phactory\Sql\Inflector::pluralize($table);
            $db->truncate($table);
        }

    }

    /**
     * Initializes builder
     *
     * @static
     * @param $db
     */
    private static function _init() {
        $db_config = self::_getDbConfig();
        $pdo = new PDO('mysql:host=' . $db_config['host'] . '; dbname=' . $db_config['database'],
            $db_config['login'], $db_config['password']);

        self::$_builder = new \Phactory\Sql\Phactory($pdo);
        self::_loadFactories();
    }

    /**
     * Loads factory definitions from Test/Factories recursively
     */
    private static function _loadFactories() {
        $factory_dir = new Folder(TESTS . 'Factories');
        foreach ($factory_dir->findRecursive() as $file_name) {
            require $file_name;
            self::$_current_table = null;
        }
    }

    private static function _getDbConfig() {
        $db = self::_getConnection();
        return $db->config;
    }

    private static function _getTablePrefix() {
        $db_config = self::_getDbConfig();
        return $db_config['prefix'];
    }

    private static function _getConnection() {
        return ConnectionManager::getDataSource(self::$_db_key);
    }
}